require 'chefspec'
require 'chefspec/berkshelf'

at_exit { ChefSpec::Coverage.report! }

RSpec.configure do |config|
  config.platform = 'ubuntu'
  config.version = '16.04'
  config.disable_monkey_patching!
end
