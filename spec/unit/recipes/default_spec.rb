require 'spec_helper'

# Currently, these unit tests only describe error scenarios in which the recipe
# fails to converge. The integration tests are the important tests for this
# cookbook.
RSpec.describe 'gitlab_disks::default' do
  context 'when an unsupported filesystem type is specified' do
    default_attributes['gitlab_disks']['persistent_mounts'] = [
      {
        'type' => 'ext2',
        'disks' => [
          '/dev/sda',
        ],
        'mountpoint' => '/tmp',
      },
    ]

    it {
      expect { chef_run }.to raise_error(/unsupported persistent mount type ext2/)
    }
  end

  context 'when 0 disks are specified for ext4' do
    default_attributes['gitlab_disks']['persistent_mounts'] = [
      {
        'type' => 'ext4',
        'mountpoint' => '/tmp',
      },
    ]

    it {
      expect { chef_run }.to raise_error(/ext4 only supports exactly one disk/)
    }
  end

  context 'when 2 zpools are specified' do
    default_attributes['gitlab_disks']['persistent_mounts'] = [
      {
        'type' => 'zfs',
        'zfs_parameters' => {
          'pool_type': 'raidz1',
        },
        'mountpoint' => '/zfs1',
      },
      {
        'type' => 'zfs',
        'zfs_parameters' => {
          'pool_type': 'raidz1',
        },
        'mountpoint' => '/zfs2',
      },
    ]

    it {
      expect { chef_run }.to raise_error 'maximum of 1 zpool per node is supported'
    }
  end

  context 'when pool_type is not specified for ZFS' do
    default_attributes['gitlab_disks']['persistent_mounts'] = [
      {
        'type' => 'zfs',
        'mountpoint' => '/zfs',
      },
    ]

    it {
      expect { chef_run }.to raise_error 'pool_type must be specified for zfs'
    }
  end
end
