require_relative '../../../libraries/zfs_helpers.rb'

RSpec.describe 'ZFS helper functions' do
  describe 'get_corrupted_cache_disks' do
    subject(:corrupted_cache_disks) do
      get_corrupted_cache_disks(
        zpool_status_output,
        %w[/dev/nvme0n1 /dev/nvme0n2 /dev/nvme0n3 /dev/nvme0n4]
      )
    end

    context 'when there are no corrupted cache disks in the pool' do
      let(:zpool_status_output) do
        <<-ZPOOL_STATUS
  pool: tank
 state: ONLINE
  scan: none requested
config:

        NAME                STATE     READ WRITE CKSUM
        tank                ONLINE       0     0     0
          raidz1-0          ONLINE       0     0     0
            google-data-01  ONLINE       0     0     0
            google-data-02  ONLINE       0     0     0
            google-data-03  ONLINE       0     0     0
            google-data-04  ONLINE       0     0     0
            google-data-05  ONLINE       0     0     0
            google-data-06  ONLINE       0     0     0
            google-data-07  ONLINE       0     0     0
            google-data-08  ONLINE       0     0     0
            google-data-09  ONLINE       0     0     0
        cache
          nvme0n1           ONLINE       0     0     0
          nvme0n2           ONLINE       0     0     0
          nvme0n3           ONLINE       0     0     0
          nvme0n4           ONLINE       0     0     0

errors: No known data errors
        ZPOOL_STATUS
      end

      it 'returns an empty array' do
        is_expected.to eq([])
      end
    end

    context 'when there are corrupted cache disks in the pool' do
      let(:zpool_status_output) do
        <<-ZPOOL_STATUS
  pool: tank
 state: ONLINE
status: One or more devices could not be used because the label is missing or
        invalid.  Sufficient replicas exist for the pool to continue
        functioning in a degraded state.
action: Replace the device using 'zpool replace'.
   see: http://zfsonlinux.org/msg/ZFS-8000-4J
  scan: none requested
config:

        NAME                STATE     READ WRITE CKSUM
        tank                ONLINE       0     0     0
          raidz1-0          ONLINE       0     0     0
            google-data-01  ONLINE       0     0     0
            google-data-02  ONLINE       0     0     0
            google-data-03  ONLINE       0     0     0
            google-data-04  ONLINE       0     0     0
            google-data-05  ONLINE       0     0     0
            google-data-06  ONLINE       0     0     0
            google-data-07  ONLINE       0     0     0
            google-data-08  ONLINE       0     0     0
            google-data-09  ONLINE       0     0     0
        cache
          nvme0n1           ONLINE       0     0     0
          nvme0n2           FAULTED      0     0     0  corrupted data
          nvme0n3           ONLINE       0     0     0
          nvme0n4           FAULTED      0     0     0  corrupted data

errors: No known data errors
        ZPOOL_STATUS
      end

      it 'returns the corrupted disks' do
        is_expected.to eq(%w[nvme0n2 nvme0n4])
      end
    end
  end
end
