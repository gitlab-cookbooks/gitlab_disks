[![build status](https://gitlab.com/gitlab-cookbooks/gitlab_disks/badges/master/build.svg)](https://gitlab.com/gitlab-cookbooks/gitlab_disks/commits/master)
[![coverage report](https://gitlab.com/gitlab-cookbooks/gitlab_disks/badges/master/coverage.svg)](https://gitlab.com/gitlab-cookbooks/gitlab_disks/commits/master)

# gitlab_disks

Idempotently formats and mounts persistent disks. Currently supports ext4 and
ZFS (single disk and raidz1 only) filesystems.

Idempotently formats and mounts persistent disks. The default recipe should be
placed first in the run list for any machine that needs it. This recipe is
compatible with any version of the bootstrap script, e.g.
[v8](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/bootstrap/blob/master/scripts/bootstrap-v8.sh),
which is also idempotent. To get the most out of this cookbook, use bootstrap
script v9 or later and set `GL_BOOTSTRAP_DATA_DISK=false`, which prevents the
bootstrap script from formatting and mounting persistent disks (apart from a
special-case log disk). [MR that adds this
feature](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/bootstrap/merge_requests/2).

## Using

See `attributes/default.rb` for configuration instructions.

## Testing

### Testing locally

You can run `rspec` or `kitchen` tests directly without using provided
`Makefile`, although you can follow instructions to benefit from it.

1. Install GNU Make (`apt-get install make`). Under OS X you can achieve the
   same by `brew install make`. After this, you can see available targets of
   the Makefile just by running `make` in cookbook directory.

1. Cheat-sheet overview of current targets:

 * `make gems`: install latest version of required gems into directory,
   specified by environmental variable `BUNDLE_PATH`. By default it is set to
   the same directory as on CI, `.bundle`, in the same directory as Makefile
   is located.

 * `make lint`: find all `*.rb` files in the current directory, excluding ones
   in `BUNDLE_PATH`, and check them with rubocop and foodcritic.

 * `make rspec`: the above, plus run all the rspec tests. You can use
   `bundle exec rspec -f d` to skip the lint step, but it is required on CI
   anyways, so rather please fix it early ;)

 * `make kitchen`: calculate the number of suites in `.kitchen.do.yml`, and
   run all integration tests, using the calculated number as a `concurrency`
   parameter. In order to this locally by default, copy the example kitchen
   config to your local one: `cp .kitchen.do.yml .kitchen.local.yml`, or
   export environmental variable: `export KITCHEN_YAML=".kitchen.do.yml"`

   *Note* that `.kitchen.yml` is left as a default Vagrant setup and is not
   used by Makefile.

1. In order to use DigitalOcean for integration testing locally, by using
   `make kitchen` or running `bundle exec kitchen test --destroy=always`,
   export the following variables according to the
   [kitchen-digitalocean](https://github.com/test-kitchen/kitchen-digitalocean)
   documentation:
  * `DIGITALOCEAN_ACCESS_TOKEN`
  * `DIGITALOCEAN_SSH_KEY_IDS`

### on CI

Alternatively, you can just push to your branch and let CI handle the testing.
To setup it, add the `DIGITALOCEAN_ACCESS_TOKEN` secret variable under your
project settings, `make kitchen` target will:
 * detect the CI environment
 * generate ephemeral SSH ed25519 keypair
 * register them on DigitalOcean
 * export the resulting key as `DIGITALOCEAN_SSH_KEY_IDS` environment variable
 * run the kitchen test
 * clean up the ephemeral key from DigitalOcean after pipeline is done

`.gitlab-ci.yml` is the source of truth for what actually runs in CI.
