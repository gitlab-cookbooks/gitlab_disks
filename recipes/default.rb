require_relative '../libraries/zfs_helpers.rb'

if node['gitlab_disks']['persistent_mounts'].select { |mount| mount['type'] == 'zfs' }.length > 1
  raise 'maximum of 1 zpool per node is supported'
end

apt_update 'apt_update'

# This should be present in the base image, but we want to be sure we have the
# right tools to test if we need to format.
package 'e2fsprogs'

# Set ZFS tunables only once, by notifying this block, since they are
# system-global and not scoped to a filesystem.
ruby_block 'set zfs tunables' do
  block do
    node['gitlab_disks']['zfs_tunables'].each do |parameter, value|
      File.open("/sys/module/zfs/parameters/#{parameter}", File::WRONLY) { |file| file.write(value.to_s) }
    end
  end

  action :nothing
end

def handle_ext4(persistent_mount)
  raise 'ext4 only supports exactly one disk' unless persistent_mount['disks']&.length == 1

  disk = persistent_mount['disks'].first
  mountpoint = persistent_mount['mountpoint']

  execute "format #{disk}" do
    # Options copied directly from the bootstrap script, which look like they
    # were mostly copied from
    # https://cloud.google.com/compute/docs/disks/performance.  The "discard"
    # option is unconditionally specified because for now we have assumed that
    # ext4 will only be used on SSDs.
    command %W(mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard #{disk})
    only_if do
      File.exist?(disk) &&
        Mixlib::ShellOut.new(%W(dumpe2fs -h #{disk})).run_command.error?
    end
  end

  directory mountpoint do
    owner 'root'
    group 'root'
    mode 0755
    recursive true
  end

  execute "mount #{disk}" do
    command %W(mount -o discard,defaults #{disk} #{mountpoint})
    only_if do
      File.exist?(disk) && Mixlib::ShellOut.new(%W(grep #{mountpoint} /proc/mounts)).run_command.error?
    end
  end

  ruby_block "add #{disk} to fstab" do
    block do
      uuid = Mixlib::ShellOut.new(%W(blkid -s UUID -o value #{disk})).run_command.stdout.strip
      fstab_editor = Chef::Util::FileEdit.new('/etc/fstab')

      # The "discard" option is unconditionally specified because for now we
      # have assumed that ext4 will only be used on SSDs.
      fstab_editor.insert_line_if_no_match(
        /^UUID=#{uuid}/,
        "UUID=#{uuid} #{mountpoint} ext4 discard,defaults 0 2"
      )
      fstab_editor.write_file
    end
  end
end

def handle_zfs(persistent_mount)
  # For single-disk / striped configurations, must explicitly pass pool_type =
  # 'disk'. This avoids accidental striping due to disconfiguration.
  if persistent_mount['zfs_parameters'].nil? || persistent_mount['zfs_parameters'].empty? || persistent_mount['zfs_parameters']['pool_type'].nil? || persistent_mount['zfs_parameters']['pool_type'].empty?
    raise 'pool_type must be specified for zfs'
  end

  package 'zfsutils-linux'

  ruby_block 'trigger set zfs tunables' do
    block {}
    notifies :run, 'ruby_block[set zfs tunables]', :delayed
  end

  mountpoint = persistent_mount['mountpoint']
  ashift = (persistent_mount['zfs_parameters']['ashift'] || 12).to_i # 12=4KiB
  recordsize_bytes = (persistent_mount['zfs_parameters']['recordsize'] || 32768).to_i # Size in byte on base 2
  recordsize = to_filesize(recordsize_bytes)
  compression = (persistent_mount['zfs_parameters']['compression'] || 'lz4').to_s
  logbias = (persistent_mount['zfs_parameters']['logbias'] || 'throughput').to_s
  atime = (persistent_mount['zfs_parameters']['atime'] || 'off').to_s
  pool_type = persistent_mount['zfs_parameters']['pool_type']
  zpool_name = 'zpool0'
  dataset_name = "#{zpool_name}/gitlab"
  reservation_percent = (persistent_mount['zfs_parameters']['reservation_percent'] || 10).to_i

  cmd = %W(zpool create -o ashift=#{ashift} -f #{zpool_name})
  cmd += [pool_type] unless pool_type == 'disk'
  cmd += persistent_mount['disks']
  cmd = cmd + %w(cache) + persistent_mount['zfs_parameters']['l2arc'] unless persistent_mount['zfs_parameters']['l2arc'].nil?

  execute "create zpool for #{mountpoint}" do
    command cmd
    only_if do
      Mixlib::ShellOut.new(%W(zpool status #{zpool_name})).run_command.error?
    end
  end

  execute "create ZFS dataset for #{mountpoint}" do
    command %W(zfs create -o mountpoint=#{mountpoint} #{dataset_name})
    only_if do
      Mixlib::ShellOut.new(%W(zfs list #{dataset_name})).run_command.error?
    end
  end

  execute "create ZFS dataset for #{zpool_name}/reservation" do
    command %W(zfs create #{zpool_name}/reservation)
    only_if do
      Mixlib::ShellOut.new(%W(zfs list #{zpool_name}/reservation)).run_command.error?
    end
  end

  ruby_block 'replace corrupt cache disks' do # ~FC022
    block do
      zpool_status_cmd = Mixlib::ShellOut.new(%W(zpool status #{zpool_name}))
      zpool_status_cmd.run_command.error!
      corrupted_cache_disks = get_corrupted_cache_disks(zpool_status_cmd.stdout, persistent_mount['zfs_parameters']['l2arc'])

      corrupted_cache_disks.each do |corrupt_cache_disk|
        Mixlib::ShellOut.new(%W(zpool remove #{zpool_name} #{corrupt_cache_disk})).run_command.error!
      end

      unless corrupted_cache_disks.empty?
        Mixlib::ShellOut.new(%W(zpool add #{zpool_name} cache) + corrupted_cache_disks).run_command.error!
      end
    end

    not_if { persistent_mount['zfs_parameters']['l2arc'].nil? }
  end

  execute "set ZFS recordsize on #{zpool_name}" do
    command %W(zfs set recordsize=#{recordsize} #{zpool_name})
    only_if do
      !Mixlib::ShellOut.new(%W(zfs get -H recordsize #{dataset_name})).run_command.stdout.to_s.include?(recordsize)
    end
  end

  execute "set ZFS compression on #{zpool_name}" do
    command %W(zfs set compression=#{compression} #{zpool_name})
    only_if do
      !Mixlib::ShellOut.new(%W(zfs get -H compression #{dataset_name})).run_command.stdout.to_s.include?(compression)
    end
  end

  execute "set ZFS logbias on #{zpool_name}" do
    command %W(zfs set logbias=#{logbias} #{zpool_name})
    only_if do
      !Mixlib::ShellOut.new(%W(zfs get -H logbias #{dataset_name})).run_command.stdout.to_s.include?(logbias)
    end
  end

  execute "set ZFS atime on #{zpool_name}" do
    command %W(zfs set atime=#{atime} #{zpool_name})
    only_if do
      !Mixlib::ShellOut.new(%W(zfs get -H atime #{dataset_name})).run_command.stdout.to_s.include?(atime)
    end
  end

  # Has to happen after the pool definitely exists
  ruby_block 'set reservation' do
    block do
      reservation_bytes = get_total_pool_space(zpool_name) / 100 * reservation_percent
      reservation_unit = to_filesize(reservation_bytes)

      unless Mixlib::ShellOut.new(%W(zfs get -H reservation #{zpool_name}/reservation)).run_command.stdout.to_s.include?(reservation_unit)
        Mixlib::ShellOut.new(%W(zfs set reservation=#{reservation_unit} #{zpool_name}/reservation)).run_command.error!
      end
    end
  end
end

node['gitlab_disks']['persistent_mounts'].each do |persistent_mount|
  directory persistent_mount['mountpoint'] do
    owner 'root'
    group 'root'
    mode 0755
    recursive true
  end

  case persistent_mount['type']
  when 'ext4'
    handle_ext4(persistent_mount)

  when 'zfs'
    handle_zfs(persistent_mount)

  else
    raise "unsupported persistent mount type #{persistent_mount['type']}"
  end
end
