name             'gitlab_disks'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'Cookbook for formatting and mounting disks'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.9'
chef_version     '>= 12.1' if respond_to?(:chef_version)
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab_users/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab_users'

supports 'ubuntu', '= 16.04'

# Please specify dependencies with version pin:
# depends 'cookbookname', '~> 1.0.0'
