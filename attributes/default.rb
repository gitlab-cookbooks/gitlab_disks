# Currently, only ext4 and zfs (single disk and raidz1) types are supported.
# Restrictions:
#
# Exactly one disk must be specified for each ext4 mountpoint. A maximum of one
# persistent_mount of type zfs may be specified.
#
# Examples:
#
# default['gitlab_disks']['persistent_mounts'] = [
#   {
#     'type' => 'ext4',
#     'disks' => [
#       '/dev/disk/by-id/google-persistent-disk-1',
#     ],
#     'mountpoint' => '/var/opt/gitlab',
#   },
#   {
#     'type' => 'zfs',
#     'disks' => [
#       '/dev/disk/by-id/google-persistent-disk-1',
#       '/dev/disk/by-id/google-persistent-disk-2',
#       '/dev/disk/by-id/google-persistent-disk-3',
#     ],
#     'mountpoint' => '/var/opt/gitlab',
#     'zfs_parameters' => {
#       'pool_type' => 'raidz1',
#       'ashift' => 12, # 12 = 4KiB
#       'compression' => 'lz4',
#       'recordsize' => 32768, # Size in byte on base 2
#       'reservation_percent' => 10,
#       'l2arc' => [
#         '/dev/disk/by-id/google-local-ssd-1',
#         '/dev/disk/by-id/google-local-ssd-2',
#       ],
#     },
#   },
# ]
#
# default['gitlab_disks']['persistent_mounts'] = [
#   {
#     'type' => 'zfs',
#     'pool_type' => 'disk',
#     'disks' => [
#       '/dev/disk/by-id/google-persistent-disk-1',
#     ],
#   },
# ]
default['gitlab_disks']['persistent_mounts'] = []

# Any dynamically settable numeric ZoL tunables can be set here
# https://github.com/zfsonlinux/zfs/wiki/ZFS-on-Linux-Module-Parameters
# (Please note the version of ZoL on the target machine!)
#
# Example:
#
# default['gitlab_disks']['zfs_tunables'] = {
#     "zfs_arc_max" => 512, # Please don't use this number, though
#     "zfs_arc_min" => 512,
# }
#
# Note that these tunables are blindly applied: Any post-update actions must be
# run manually. For example, if you shrink zfs_arc_max, you must run
# `echo 3 > /proc/sys/vm/drop_caches`.
default['gitlab_disks']['zfs_tunables'] = {}
