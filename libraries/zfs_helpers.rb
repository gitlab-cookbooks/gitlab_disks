def get_corrupted_cache_disks(zpool_status_output, cache_disks)
  # Assume that L2ARC cache disks are all "/dev/something". If this is not the
  # case, the worst that can happen is that this function returns an empty array
  # and we do not attempt to repair the corrupted cache. The zpool should
  # continue to function with slower reads.
  # This cookbook will soon be adding a ZFS prometheus exporter, and we intend
  # to alert on persistent bad states.
  cache_disks.map { |disk| File.basename(disk) }.select do |disk|
    /^\s+#{disk}\s+FAULTED.+corrupted\sdata$/.match(zpool_status_output)
  end
end

def get_total_pool_space(zpool_name)
  df_cmd = Mixlib::ShellOut.new(%W(zpool get -pH size #{zpool_name}))
  result = df_cmd.run_command
  result.error!
  splits = result.stdout.to_s.split(/\s/)
  size = splits[2]
  return size.to_i unless size.to_i == 0
  raise "pool size for #{zpool_name} counld not be found"
end

def to_filesize(bytes)
  {
    '' => 1024,
    'K' => 1024 * 1024,
    'M' => 1024 * 1024 * 1024,
    'G' => 1024 * 1024 * 1024 * 1024,
    'T' => 1024 * 1024 * 1024 * 1024 * 1024,
  }.each_pair { |e, s| return "#{(bytes.to_f / (s.to_f / 1024)).round(0)}#{e}" if bytes < s }.to_i
end
