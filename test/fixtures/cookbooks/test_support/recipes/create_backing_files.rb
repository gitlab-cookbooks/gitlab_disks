0.upto(6) do |i|
  backing_file = "/backing#{i}"
  execute "create backing file #{i}" do
    command "fallocate -l 100M #{backing_file}"
    not_if { File.exist?(backing_file) }
  end
end
