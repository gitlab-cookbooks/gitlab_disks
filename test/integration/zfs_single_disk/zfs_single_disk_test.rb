control 'zfs single disk test' do
  title 'zfs single disk test'

  describe mount '/media/zfs' do
    it { is_expected.to be_mounted }
    its('type') { is_expected.to eq 'zfs' }
    its('device') { is_expected.to eq 'zpool0/gitlab' }
  end

  describe mount '/zpool0/reservation' do
    it { is_expected.to be_mounted }
    its('type') { is_expected.to eq 'zfs' }
    its('device') { is_expected.to eq 'zpool0/reservation' }
  end

  describe command 'zdb' do
    its(:exit_status) { is_expected.to eq 0 }
    its(:stdout) { is_expected.to include 'ashift: 12' }
  end

  describe command 'zfs get compression zpool0' do
    its(:stdout) { is_expected.to match(/lz4\s+local/) }
  end

  describe command 'zfs get recordsize zpool0' do
    its(:stdout) { is_expected.to match(/32K\s+local/) }
  end

  describe command 'zfs get reservation zpool0/reservation' do
    # 20% of usable space
    its(:stdout) { is_expected.to match(/8M\s+local/) }
  end

  describe file '/sys/module/zfs/parameters/zfs_arc_max' do
    # The default value. Not actually 0:
    # https://github.com/zfsonlinux/zfs/wiki/ZFS-on-Linux-Module-Parameters#zfs_arc_max
    its(:content) { is_expected.to eq "0\n" }
  end
end
