control 'disks tests' do
  title 'disks tests'

  # This should be present in the base image, but we want to be sure we have the
  # right tools to test if we need to format.
  describe command 'dumpe2fs' do
    it { is_expected.to exist }
  end

  describe mount '/media/data1' do
    it { is_expected.to be_mounted }
    its('type') { is_expected.to eq 'ext4' }
    its('device') { is_expected.to eq '/backing1' }
  end

  describe etc_fstab.where { mount_point == '/media/data1' } do
    it { is_expected.to be_configured }
    its('file_system_type') { is_expected.to eq ['ext4'] }
  end

  describe mount '/media/data2' do
    it { is_expected.to be_mounted }
    its('type') { is_expected.to eq 'ext4' }
    its('device') { is_expected.to eq '/backing2' }
  end

  describe etc_fstab.where { mount_point == '/media/data2' } do
    it { is_expected.to be_configured }
    its('file_system_type') { is_expected.to eq ['ext4'] }
  end

  describe mount '/media/raidz' do
    it { is_expected.to be_mounted }
    its('type') { is_expected.to eq 'zfs' }
    its('device') { is_expected.to eq 'zpool0/gitlab' }
  end

  describe command 'zdb' do
    its(:exit_status) { is_expected.to eq 0 }
    its(:stdout) { is_expected.to include 'ashift: 9' }
  end

  describe command 'zfs get compression zpool0' do
    its(:stdout) { is_expected.to match(/\s+off\s+default/) }
  end

  describe command 'zfs get recordsize zpool0' do
    its(:stdout) { is_expected.to match(/64K\s+local/) }
  end

  describe command 'zfs get reservation zpool0/reservation' do
    # 20% of usable space
    its(:stdout) { is_expected.to match(/54M\s+local/) }
  end

  describe file '/sys/module/zfs/parameters/zfs_arc_max' do
    its(:content) { is_expected.to eq "123456789\n" }
  end
end
